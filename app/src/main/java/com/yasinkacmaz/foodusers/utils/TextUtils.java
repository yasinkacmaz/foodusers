package com.yasinkacmaz.foodusers.utils;

public class TextUtils {

  public static String generateFullName(String fullname) {
    String result = "";

    if (fullname.contains(" ")) {
      for (String split : fullname.split(" ")) {
        if (!split.isEmpty()) {
          result += split.substring(0, 1).toUpperCase() + split.substring(1).toLowerCase() + " ";
        }
      }
    } else {
      if (!fullname.isEmpty()) {
        result += fullname.substring(0, 1).toUpperCase() + fullname.substring(1).toLowerCase();
      }
    }

    return result.trim();
  }
}
