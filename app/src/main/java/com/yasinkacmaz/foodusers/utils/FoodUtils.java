package com.yasinkacmaz.foodusers.utils;

import java.util.Random;

public class FoodUtils {

  private static String[] foods = {
      "Apple", "Avacado", "Bacon", "Black Beans", "Bread", "Buritto", "Cabbage", "Cake", "Celery",
      "Cheese", "Chicken", "Chocolate", "Duck", "Donut", "Egg", "Fajita", "Falafel", "Fish",
      "Ginger", "Grapes", "Ham", "Honey", "Hot Dog", "Lamb", "Lasagna", "Meatball", "Milkshake",
      "Pizza", "Pancake", "Spinach", "Spaghetti"
  };

  public static String getRandomFood() {
    Random rnd = new Random();
    return foods[rnd.nextInt(foods.length)];
  }
}
