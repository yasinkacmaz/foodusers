package com.yasinkacmaz.foodusers.users;

public interface UsersAdapterInterface {
  void onItemClicked(int userId, String userPhotoUrl);
}
