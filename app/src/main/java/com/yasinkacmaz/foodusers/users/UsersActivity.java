package com.yasinkacmaz.foodusers.users;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import com.yasinkacmaz.foodusers.R;
import com.yasinkacmaz.foodusers.base.BaseActivity;
import com.yasinkacmaz.foodusers.userdetail.UserDetailActivity;
import com.yasinkacmaz.foodusers.widget.WrapContentLinearLayoutManager;
import java.util.List;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;

public class UsersActivity extends BaseActivity
    implements UsersContract.View, UsersAdapterInterface {

  @BindView(R.id.toolbar) Toolbar toolbar;
  @BindView(R.id.activity_users_recylerview) RecyclerView recyclerView;
  @Inject UsersContract.Presenter usersPresenter;

  WrapContentLinearLayoutManager layoutManager;
  UsersAdapter usersAdapter;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_users);

    ButterKnife.bind(this);

    setSupportActionBar(toolbar);
    if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    usersPresenter.start();
    initRecyclerView();
    usersPresenter.getUsers();
  }

  public void initRecyclerView() {
    layoutManager = new WrapContentLinearLayoutManager(getApplicationContext());
    recyclerView.setLayoutManager(layoutManager);
    recyclerView.setHasFixedSize(true);

    usersAdapter = new UsersAdapter(getApplicationContext(), this);
    recyclerView.setAdapter(usersAdapter);
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        onBackPressed();
        return true;
    }

    return (super.onOptionsItemSelected(item));
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    usersPresenter.stop();
  }

  @Override public void onBackPressed() {
    moveTaskToBack(true);
  }

  @Override public void getUsersSuccess(List<User> users) {
    usersAdapter.addAll(users);
  }

  @Override public void getUsersError(String errorMessage) {
    longToast(errorMessage);
  }

  @Override public void onItemClicked(int userId, String userPhotoUrl) {
    Intent detailIntent = new Intent(thisActivity, UserDetailActivity.class);
    detailIntent.putExtra(getStringShort(R.string.user_id_intent_extra), userId);
    detailIntent.putExtra(getStringShort(R.string.user_photo_intent_extra), userPhotoUrl);
    startActivity(detailIntent);
  }
}
