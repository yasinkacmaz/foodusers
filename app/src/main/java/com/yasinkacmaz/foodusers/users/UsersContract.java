package com.yasinkacmaz.foodusers.users;

import com.yasinkacmaz.foodusers.base.BasePresenter;
import java.util.List;
import io.reactivex.Observable;
import retrofit2.Response;

public interface UsersContract {
  interface View {
    void getUsersSuccess(List<User> users);

    void getUsersError(String errorMessage);
  }

  interface Presenter extends BasePresenter {
    void getUsers();
  }

  interface Model {
    Observable<Response<UsersResponse>> getUsers();
  }
}
