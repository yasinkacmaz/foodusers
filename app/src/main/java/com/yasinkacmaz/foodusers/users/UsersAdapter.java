package com.yasinkacmaz.foodusers.users;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.util.SortedList;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.util.SortedListAdapterCallback;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.squareup.picasso.Picasso;
import com.yasinkacmaz.foodusers.R;
import com.yasinkacmaz.foodusers.utils.FoodUtils;
import com.yasinkacmaz.foodusers.utils.TextUtils;
import de.hdodenhof.circleimageview.CircleImageView;
import java.util.List;

@SuppressWarnings("WeakerAccess") public class UsersAdapter extends RecyclerView.Adapter {
  private final int VIEW_TYPE_ITEM = 0;
  private final int VIEW_TYPE_EMPTY = 1;
  private SortedList<User> users;
  private Picasso picasso;
  private Context context;
  private UsersAdapterInterface usersAdapterInterface;

  public UsersAdapter(Context context, @Nullable UsersAdapterInterface usersAdapterInterface) {
    picasso = Picasso.with(context);
    this.context = context;
    this.usersAdapterInterface = usersAdapterInterface;
    initList();
  }

  private void initList() {
    users = new SortedList<>(User.class, new SortedListAdapterCallback<User>(this) {

      @Override public int compare(User o1, User o2) {
        return o1.getId().compareTo(o2.getId());
      }

      @Override public boolean areContentsTheSame(User oldItem, User newItem) {
        return false;
      }

      @Override public boolean areItemsTheSame(User item1, User item2) {
        return false;
      }
    });
  }

  public class UserView extends RecyclerView.ViewHolder {
    @BindView(R.id.users_row_container) CardView container;
    @BindView(R.id.users_row_name) TextView fullName;
    @BindView(R.id.users_row_description) TextView description;
    @BindView(R.id.users_row_image) CircleImageView imageView;

    public UserView(View view) {
      super(view);
      ButterKnife.bind(this, view);
    }
  }

  public class EmptyView extends RecyclerView.ViewHolder {

    public EmptyView(View itemView) {
      super(itemView);
    }
  }

  @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    if (viewType == VIEW_TYPE_ITEM) {
      View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_row, parent, false);
      return new UserView(row);
    } else if (viewType == VIEW_TYPE_EMPTY) {
      View row =
          LayoutInflater.from(parent.getContext()).inflate(R.layout.user_empty_row, parent, false);
      return new EmptyView(row);
    }
    return null;
  }

  @Override public int getItemCount() {
    if (users.size() == 0) {
      return 1;
    } else {
      return users.size();
    }
  }

  @Override public int getItemViewType(int position) {
    if (users.size() == 0) {
      return VIEW_TYPE_EMPTY;
    } else {
      return VIEW_TYPE_ITEM;
    }
  }

  @Override public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
    int safePosition = holder.getAdapterPosition();
    if (safePosition >= 0 && safePosition < getItemCount() && getItemCount() > 0) {
      if (holder instanceof UserView) {
        final UserView myHolder = (UserView) holder;
        User userModel = get(safePosition);
        myHolder.fullName.setText(
            TextUtils.generateFullName(userModel.getFirstName() + " " + userModel.getLastName()));
        myHolder.description.setText(
            String.format(context.getString(R.string.favourite_food), FoodUtils.getRandomFood()));
        picasso.load(userModel.getAvatar()).into(myHolder.imageView);
        myHolder.container.setOnClickListener(view -> {
          if (usersAdapterInterface != null) {
            usersAdapterInterface.onItemClicked(userModel.getId(), userModel.getAvatar());
          }
        });
      }
    }
  }

  public User get(int position) {
    return users.get(position);
  }

  public int add(User item) {
    if (users.size() == 0) initList();
    return users.add(item);
  }

  public int indexOf(User item) {
    return users.indexOf(item);
  }

  public void updateItemAt(int index, User item) {
    users.updateItemAt(index, item);
  }

  public void addAll(List<User> items) {
    users.beginBatchedUpdates();
    for (User item : items) {
      users.add(item);
    }
    users.endBatchedUpdates();
  }
}
