package com.yasinkacmaz.foodusers.users;

import com.yasinkacmaz.foodusers.constants.NetworkConstants;
import com.yasinkacmaz.foodusers.service.FoodApi;
import javax.inject.Inject;
import io.reactivex.Observable;
import retrofit2.Response;

public class UsersModel implements UsersContract.Model {
  private FoodApi foodApi;

  @Inject public UsersModel(FoodApi foodApi) {
    this.foodApi = foodApi;
  }

  @Override public Observable<Response<UsersResponse>> getUsers() {
    return foodApi.getUsers(NetworkConstants.TAKE_COUNT, NetworkConstants.DELAY_SECONDS);
  }
}
