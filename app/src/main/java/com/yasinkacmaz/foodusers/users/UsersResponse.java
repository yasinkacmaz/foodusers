package com.yasinkacmaz.foodusers.users;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

public class UsersResponse {
  @SerializedName("page") @Expose private String page;
  @SerializedName("per_page") @Expose private String perPage;
  @SerializedName("total") @Expose private Integer total;
  @SerializedName("total_pages") @Expose private Integer totalPages;
  @SerializedName("data") @Expose private List<User> data = new ArrayList<>();

  public String getPage() {
    return page;
  }

  public void setPage(String page) {
    this.page = page;
  }

  public String getPerPage() {
    return perPage;
  }

  public void setPerPage(String perPage) {
    this.perPage = perPage;
  }

  public Integer getTotal() {
    return total;
  }

  public void setTotal(Integer total) {
    this.total = total;
  }

  public Integer getTotalPages() {
    return totalPages;
  }

  public void setTotalPages(Integer totalPages) {
    this.totalPages = totalPages;
  }

  public List<User> getData() {
    return data;
  }

  public void setData(List<User> data) {
    this.data = data;
  }
}
