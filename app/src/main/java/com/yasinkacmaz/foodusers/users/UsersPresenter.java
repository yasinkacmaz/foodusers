package com.yasinkacmaz.foodusers.users;

import com.yasinkacmaz.foodusers.base.BasePresenterImpl;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import javax.inject.Inject;

public class UsersPresenter extends BasePresenterImpl implements UsersContract.Presenter {

  private UsersContract.View usersView;
  private UsersContract.Model usersModel;

  @Inject public UsersPresenter(UsersContract.View usersView, UsersContract.Model usersModel) {
    this.usersView = usersView;
    this.usersModel = usersModel;
  }

  @Override public void getUsers() {
    usersModel.getUsers()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .doOnSubscribe(this::addDisposable)
        .subscribe(response -> {
          if (response.isSuccessful()) {
            usersView.getUsersSuccess(response.body().getData());
          } else {
            usersView.getUsersError(response.errorBody().string());
          }
        }, error -> usersView.getUsersError(error.getMessage()), () -> {
        });
  }
}
