package com.yasinkacmaz.foodusers.application;

import com.yasinkacmaz.foodusers.injection.component.ApplicationComponent;
import com.yasinkacmaz.foodusers.injection.component.DaggerApplicationComponent;
import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

public class FoodUsers extends DaggerApplication {

  private ApplicationComponent applicationComponent;

  @Override public void onCreate() {
    applicationComponent = DaggerApplicationComponent.builder().application(this).build();
    super.onCreate();
  }

  @Override protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
    return applicationComponent;
  }
}
