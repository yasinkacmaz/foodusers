package com.yasinkacmaz.foodusers.splash;

import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.yasinkacmaz.foodusers.R;
import com.yasinkacmaz.foodusers.base.BaseActivity;
import com.yasinkacmaz.foodusers.login.LoginActivity;

@SuppressWarnings("FieldCanBeLocal") public class SplashActivity extends AppCompatActivity {

  private final int SPLASH_DELAY_MS = 750;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_splash);

    new Handler().postDelayed(
        () -> startActivity(new Intent(SplashActivity.this, LoginActivity.class)), SPLASH_DELAY_MS);
  }

  @Override public void onBackPressed() {
    //at least launch app once
  }
}
