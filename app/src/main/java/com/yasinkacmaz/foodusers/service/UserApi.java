package com.yasinkacmaz.foodusers.service;

import com.yasinkacmaz.foodusers.userdetail.response.UserDetailResponse;
import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface UserApi {
  @GET("users/{id}") Observable<Response<UserDetailResponse>> getUserDetails(
      @Path("id") int userId);
}
