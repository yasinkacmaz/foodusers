package com.yasinkacmaz.foodusers.service;

import com.yasinkacmaz.foodusers.login.LoginResponse;
import com.yasinkacmaz.foodusers.users.UsersResponse;
import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface FoodApi {

  @FormUrlEncoded @POST("login") Observable<Response<LoginResponse>> loginUser(
      @Field("username") String username, @Field("password") String password,
      @Query("delay") int delaySeconds);

  @GET("users") Observable<Response<UsersResponse>> getUsers(@Query("per_page") int takeCount,
      @Query("delay") int delaySeconds);
}
