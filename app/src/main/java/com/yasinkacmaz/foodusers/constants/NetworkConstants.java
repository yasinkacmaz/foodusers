package com.yasinkacmaz.foodusers.constants;

public class NetworkConstants {
  public static final String ACCEPT = "Accept";
  public static final String CONTENT_TYPE = "Content-Type";
  public static String APPLICATION_JSON = "application/json";

  public static int CONNECT_TIMEOUT = 60;
  public static int WRITE_TIMEOUT = 60;
  public static int READ_TIMEOUT = 60;

  public static int DELAY_SECONDS = 2;
  public static int TAKE_COUNT = 10;
}
