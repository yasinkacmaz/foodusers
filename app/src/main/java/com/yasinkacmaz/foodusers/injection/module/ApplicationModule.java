package com.yasinkacmaz.foodusers.injection.module;

import android.app.Application;
import android.content.Context;
import com.yasinkacmaz.foodusers.injection.qualifier.ApplicationContext;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

@Module public abstract class ApplicationModule {

  @Provides @Singleton @ApplicationContext
  static Context provideApplicationContext(Application application){
    return application.getApplicationContext();
  }
}
