package com.yasinkacmaz.foodusers.injection.qualifier;

import javax.inject.Qualifier;

@Qualifier
public @interface ApplicationContext {
}
