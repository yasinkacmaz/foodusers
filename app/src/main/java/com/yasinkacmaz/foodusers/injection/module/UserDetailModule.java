package com.yasinkacmaz.foodusers.injection.module;

import com.yasinkacmaz.foodusers.injection.scope.ActivityScope;
import com.yasinkacmaz.foodusers.userdetail.UserDetailActivity;
import com.yasinkacmaz.foodusers.userdetail.UserDetailContract;
import com.yasinkacmaz.foodusers.userdetail.UserDetailModel;
import com.yasinkacmaz.foodusers.userdetail.UserDetailPresenter;
import dagger.Binds;
import dagger.Module;

@Module public abstract class UserDetailModule {
  @Binds @ActivityScope
  abstract UserDetailContract.View provideUserDetailView(UserDetailActivity userDetailActivity);

  @Binds @ActivityScope
  abstract UserDetailContract.Model provideUserDetailModel(UserDetailModel userDetailModel);

  @Binds @ActivityScope abstract UserDetailContract.Presenter provideUserDetailPresenter(
      UserDetailPresenter userDetailPresenter);
}
