package com.yasinkacmaz.foodusers.injection.module;

import android.content.Context;
import com.google.gson.Gson;
import com.yasinkacmaz.foodusers.R;
import com.yasinkacmaz.foodusers.injection.qualifier.ApplicationContext;
import com.yasinkacmaz.foodusers.injection.qualifier.FoodRetrofit;
import com.yasinkacmaz.foodusers.injection.qualifier.UserRetrofit;
import com.yasinkacmaz.foodusers.service.FoodApi;
import com.yasinkacmaz.foodusers.service.UserApi;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = NetworkModule.class) public abstract class ServiceModule {

  @Provides @Singleton static FoodApi provideApiForFood(@FoodRetrofit Retrofit retrofit) {
    return retrofit.create(FoodApi.class);
  }

  @Provides @Singleton static UserApi provideApiForUser(@UserRetrofit Retrofit retrofit) {
    return retrofit.create(UserApi.class);
  }

  @Provides @FoodRetrofit @Singleton
  static Retrofit provideRetrofitForFoodApi(Gson gson, OkHttpClient okHttpClient,
      @ApplicationContext Context context) {
    return new Retrofit.Builder().baseUrl(context.getString(R.string.base_url_food_api))
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .client(okHttpClient)
        .build();
  }

  @Provides @UserRetrofit @Singleton
  static Retrofit provideRetrofitForUserApi(Gson gson, OkHttpClient okHttpClient,
      @ApplicationContext Context context) {
    return new Retrofit.Builder().baseUrl(context.getString(R.string.base_url_user_api))
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .client(okHttpClient)
        .build();
  }
}
