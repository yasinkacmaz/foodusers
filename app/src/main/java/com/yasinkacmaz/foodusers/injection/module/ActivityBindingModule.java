package com.yasinkacmaz.foodusers.injection.module;

import com.yasinkacmaz.foodusers.injection.scope.ActivityScope;
import com.yasinkacmaz.foodusers.login.LoginActivity;
import com.yasinkacmaz.foodusers.userdetail.UserDetailActivity;
import com.yasinkacmaz.foodusers.users.UsersActivity;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module public abstract class ActivityBindingModule {

  @ActivityScope @ContributesAndroidInjector(modules = LoginModule.class)
  abstract LoginActivity loginActivity();

  @ActivityScope @ContributesAndroidInjector(modules = UsersModule.class)
  abstract UsersActivity usersActivity();

  @ActivityScope @ContributesAndroidInjector(modules = UserDetailModule.class)
  abstract UserDetailActivity userDetailActivity();
}
