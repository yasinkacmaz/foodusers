package com.yasinkacmaz.foodusers.injection.module;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.yasinkacmaz.foodusers.constants.NetworkConstants;
import java.util.concurrent.TimeUnit;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import okhttp3.Dispatcher;
import okhttp3.OkHttpClient;
import okhttp3.Request;

@Module(includes = ApplicationModule.class) public abstract class NetworkModule {

  @Provides @Singleton static Gson provideGson() {
    return new GsonBuilder().setLenient().create();
  }

  @Provides @Singleton static Dispatcher provideDispatcher() {
    Dispatcher dispatcher = new Dispatcher();
    dispatcher.setMaxRequests(1);
    return dispatcher;
  }

  @Provides @Singleton static OkHttpClient provideOkHttpClient(Dispatcher dispatcher) {
    return new OkHttpClient.Builder().connectTimeout(NetworkConstants.CONNECT_TIMEOUT,
        TimeUnit.SECONDS)
        .readTimeout(NetworkConstants.READ_TIMEOUT, TimeUnit.SECONDS)
        .writeTimeout(NetworkConstants.WRITE_TIMEOUT, TimeUnit.SECONDS)
        .dispatcher(dispatcher)
        .addInterceptor(chain -> {
          Request request = chain.request()
              .newBuilder()
              .addHeader(NetworkConstants.ACCEPT, NetworkConstants.APPLICATION_JSON)
              .addHeader(NetworkConstants.CONTENT_TYPE, NetworkConstants.APPLICATION_JSON)
              .build();
          return chain.proceed(request);
        })
        .build();
  }
}
