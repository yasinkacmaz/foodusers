package com.yasinkacmaz.foodusers.injection.component;

import android.app.Application;
import com.yasinkacmaz.foodusers.application.FoodUsers;
import com.yasinkacmaz.foodusers.injection.module.ActivityBindingModule;
import com.yasinkacmaz.foodusers.injection.module.ApplicationModule;
import com.yasinkacmaz.foodusers.injection.module.NetworkModule;
import com.yasinkacmaz.foodusers.injection.module.ServiceModule;
import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import javax.inject.Singleton;

@Singleton @Component(modules = {
    ApplicationModule.class, NetworkModule.class, ServiceModule.class, ActivityBindingModule.class,
    AndroidSupportInjectionModule.class
}) public interface ApplicationComponent extends AndroidInjector<FoodUsers> {

  @Component.Builder interface Builder {
    @BindsInstance ApplicationComponent.Builder application(Application application);

    ApplicationComponent build();
  }
}
