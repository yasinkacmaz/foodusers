package com.yasinkacmaz.foodusers.injection.module;

import com.yasinkacmaz.foodusers.injection.scope.ActivityScope;
import com.yasinkacmaz.foodusers.login.LoginActivity;
import com.yasinkacmaz.foodusers.login.LoginContract;
import com.yasinkacmaz.foodusers.login.LoginModel;
import com.yasinkacmaz.foodusers.login.LoginPresenter;
import dagger.Binds;
import dagger.Module;

@Module public abstract class LoginModule {
  @Binds @ActivityScope abstract LoginContract.View provideLoginView(LoginActivity loginActivity);

  @Binds @ActivityScope abstract LoginContract.Model provideLoginModel(LoginModel loginModel);

  @Binds @ActivityScope
  public abstract LoginContract.Presenter provideLoginPresenter(LoginPresenter loginPresenter);
}
