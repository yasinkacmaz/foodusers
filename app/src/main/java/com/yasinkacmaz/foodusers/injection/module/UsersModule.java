package com.yasinkacmaz.foodusers.injection.module;

import com.yasinkacmaz.foodusers.injection.scope.ActivityScope;
import com.yasinkacmaz.foodusers.users.UsersActivity;
import com.yasinkacmaz.foodusers.users.UsersContract;
import com.yasinkacmaz.foodusers.users.UsersModel;
import com.yasinkacmaz.foodusers.users.UsersPresenter;
import dagger.Binds;
import dagger.Module;

@Module public abstract class UsersModule {
  @Binds @ActivityScope abstract UsersContract.View provideUsersView(UsersActivity usersActivity);

  @Binds @ActivityScope abstract UsersContract.Model provideUsersModel(UsersModel usersModel);

  @Binds @ActivityScope
  public abstract UsersContract.Presenter provideUsersPresenter(UsersPresenter usersPresenter);
}
