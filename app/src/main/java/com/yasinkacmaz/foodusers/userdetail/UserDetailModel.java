package com.yasinkacmaz.foodusers.userdetail;

import com.yasinkacmaz.foodusers.constants.NetworkConstants;
import com.yasinkacmaz.foodusers.service.UserApi;
import com.yasinkacmaz.foodusers.userdetail.response.Address;
import com.yasinkacmaz.foodusers.userdetail.response.Company;
import com.yasinkacmaz.foodusers.userdetail.response.UserDetailResponse;
import com.yasinkacmaz.foodusers.utils.TextUtils;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import io.reactivex.Observable;
import retrofit2.Response;

public class UserDetailModel implements UserDetailContract.Model {

  private UserApi userApi;
  UserDetail userDetail;

  @Inject public UserDetailModel(UserApi userApi) {
    this.userApi = userApi;
  }

  @Override public Observable<Response<UserDetailResponse>> getUserDetail(int userId) {
    return userApi.getUserDetails(userId).delay(NetworkConstants.DELAY_SECONDS, TimeUnit.SECONDS);
  }

  @Override public Observable<UserDetail> convertUserDetailToModel(
      UserDetailResponse userDetailResponse) {
    UserDetail userDetailModel = new UserDetail();
    userDetailModel.setFullname(TextUtils.generateFullName(
        userDetailResponse.getName() != null ? userDetailResponse.getName() : ""));
    userDetailModel.setEmail(
        userDetailResponse.getEmail() != null ? userDetailResponse.getEmail() : "");
    userDetailModel.setWebsite(
        userDetailResponse.getWebsite() != null ? userDetailResponse.getWebsite() : "");
    userDetailModel.setPhone(
        userDetailResponse.getPhone() != null ? userDetailResponse.getPhone() : "");

    Address address = new Address();
    address.setStreet("");
    address.setSuite("");
    address.setCity("");
    address.setZipcode("");
    if (userDetailResponse.getAddress() != null) address = userDetailResponse.getAddress();
    userDetailModel.setAddress(
        address.getSuite() + " " + address.getStreet() + " " + address.getCity());

    Company company = new Company();
    company.setCatchPhrase("");
    company.setName("");
    company.setBs("");
    if (userDetailResponse.getCompany() != null) company = userDetailResponse.getCompany();
    userDetailModel.setCompany(company.getName());
    this.userDetail = userDetailModel;
    return Observable.just(userDetailModel);
  }

  /**
   * @param website website
   * @param address address
   * @param phone phone
   * @param company company
   * @return returns false if model is null or none of these params changed, otherwise returns true
   */
  @Override public boolean isChanged(String website, String address, String phone, String company) {
    return userDetail != null && (!website.trim().equals(userDetail.getWebsite())
        || !address.trim().equals(userDetail.getAddress())
        || !phone.trim().equals(userDetail.getPhone())
        || !company.trim().equals(userDetail.getCompany()));
  }

  @Override
  public Observable<String> updateUserProfile(String website, String address, String phone,
      String company) {
    Observable<String> observable = Observable.just("");
    return observable.delay(NetworkConstants.DELAY_SECONDS, TimeUnit.SECONDS);
  }
}
