package com.yasinkacmaz.foodusers.userdetail;

import com.yasinkacmaz.foodusers.base.BasePresenter;
import com.yasinkacmaz.foodusers.base.BaseView;
import com.yasinkacmaz.foodusers.userdetail.response.UserDetailResponse;
import io.reactivex.Observable;
import retrofit2.Response;

public interface UserDetailContract {
  interface View extends BaseView {
    void setUserId(int userId);

    int getUserId();

    void setUserPhotoUrl(String userPhotoUrl);

    String getUserPhotoUrl();

    void displayPleaseWaitForUpdate();

    void getUserDetailsSuccess(UserDetail userDetailModel);

    void getUserDetailsError(String errorMessage);

    void updateUserProfileSuccess();

    void updateUserProfileError(String errorMessage);

    void navigateBack();
  }

  interface Presenter extends BasePresenter {
    void getUserDetails();

    void checkChanges(String website, String address, String phone, String company);
  }

  interface Model {

    Observable<Response<UserDetailResponse>> getUserDetail(int userId);

    Observable<UserDetail> convertUserDetailToModel(UserDetailResponse userDetailResponse);

    boolean isChanged(String website, String address, String phone, String company);

    Observable<String> updateUserProfile(String website, String address, String phone,
        String company);
  }
}
