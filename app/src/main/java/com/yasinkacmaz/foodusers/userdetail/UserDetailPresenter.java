package com.yasinkacmaz.foodusers.userdetail;

import com.yasinkacmaz.foodusers.base.BasePresenterImpl;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import javax.inject.Inject;

public class UserDetailPresenter extends BasePresenterImpl implements UserDetailContract.Presenter {

  private UserDetailContract.View userDetailView;
  private UserDetailContract.Model userDetailModel;

  @Inject public UserDetailPresenter(UserDetailContract.View userDetailView,
      UserDetailContract.Model userDetailModel) {
    this.userDetailView = userDetailView;
    this.userDetailModel = userDetailModel;
  }

  @Override public void getUserDetails() {
    userDetailModel.getUserDetail(userDetailView.getUserId()).flatMap(userDetailResponse -> {
      if (userDetailResponse.isSuccessful()) {
        return userDetailModel.convertUserDetailToModel(userDetailResponse.body());
      } else {
        return Observable.error(new Throwable(userDetailResponse.errorBody().string()));
      }
    }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).doOnSubscribe(d -> {
      userDetailView.displayPleaseWait();
      addDisposable(d);
    }).subscribe(userDetailModel -> {
      userDetailView.hidePleaseWait();
      userDetailView.getUserDetailsSuccess(userDetailModel);
    }, error -> {
      userDetailView.hidePleaseWait();
      userDetailView.getUserDetailsError(error.getMessage());
    }, () -> {
    });
  }

  @Override public void checkChanges(String website, String address, String phone, String company) {
    if (!userDetailModel.isChanged(website, address, phone, company)) {
      userDetailView.navigateBack();
    } else {
      userDetailModel.updateUserProfile(website, address, phone, company)
          .subscribeOn(Schedulers.io())
          .observeOn(AndroidSchedulers.mainThread())
          .doOnSubscribe(d -> {
            userDetailView.displayPleaseWaitForUpdate();
            addDisposable(d);
          })
          .subscribe(s -> {
            userDetailView.hidePleaseWait();
            userDetailView.updateUserProfileSuccess();
            userDetailView.navigateBack();
          }, error -> {
            userDetailView.hidePleaseWait();
            userDetailView.updateUserProfileError(error.getMessage());
            userDetailView.navigateBack();
          }, () -> {
          });
    }
  }
}
