package com.yasinkacmaz.foodusers.userdetail;

import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.yasinkacmaz.foodusers.R;
import com.yasinkacmaz.foodusers.base.BaseActivity;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;

public class UserDetailActivity extends BaseActivity implements UserDetailContract.View {

  @BindView(R.id.toolbar) Toolbar toolbar;
  @BindView(R.id.user_detail_appbar) AppBarLayout appBarLayout;
  @BindView(R.id.user_detail_collapsing_toolbar) CollapsingToolbarLayout collapsingToolbarLayout;
  @BindView(R.id.user_detail_photo) ImageView collapsingImageView;
  @BindView(R.id.user_detail_fullname) MaterialEditText editTextFullName;
  @BindView(R.id.user_detail_email) MaterialEditText editTextEmail;
  @BindView(R.id.user_detail_website) MaterialEditText editTextWebsite;
  @BindView(R.id.user_detail_address) MaterialEditText editTextAddress;
  @BindView(R.id.user_detail_phone) MaterialEditText editTextPhone;
  @BindView(R.id.user_detail_company) MaterialEditText editTextCompany;

  @Inject UserDetailContract.Presenter userDetailPresenter;
  private int userId;
  private String photoUrl;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_user_detail);

    ButterKnife.bind(this);

    setSupportActionBar(toolbar);
    if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    initViews();
    userDetailPresenter.start();
    handleExtras();
  }

  void initViews() {
    appBarLayout.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
      float offsetAlpha = (appBarLayout.getY() / (this.appBarLayout.getTotalScrollRange()));
      collapsingImageView.setAlpha(1 - (offsetAlpha * -1.2f));
    });
  }

  void handleExtras() {
    Bundle extras = getIntent().getExtras();
    if (extras != null) {
      setUserId(extras.getInt(getStringShort(R.string.user_id_intent_extra), 1));
      setUserPhotoUrl(extras.getString(getStringShort(R.string.user_photo_intent_extra), ""));
      userDetailPresenter.getUserDetails();
    } else {
      longToast(R.string.unable_to_get_user_details);
      onBackPressed();
    }
  }

  @Override public void setUserId(int userId) {
    this.userId = userId;
  }

  @Override public int getUserId() {
    return userId;
  }

  @Override public void setUserPhotoUrl(String userPhotoUrl) {
    this.photoUrl = userPhotoUrl;
  }

  @Override public String getUserPhotoUrl() {
    return photoUrl;
  }

  @Override public void getUserDetailsSuccess(UserDetail userDetailModel) {
    Picasso.with(getApplicationContext())
        .load(getUserPhotoUrl())
        .into(collapsingImageView, new Callback() {
          @Override public void onSuccess() {
            Palette.from(((BitmapDrawable) collapsingImageView.getDrawable()).getBitmap())
                .generate(palette -> {
                  int mutedColor = palette.getMutedColor(getColorShort(R.color.colorPrimaryDark));
                  appBarLayout.setBackgroundColor(mutedColor);

                  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Window window = getWindow();
                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    window.setStatusBarColor(mutedColor);
                  }
                });
          }

          @Override public void onError() {
            collapsingImageView.setVisibility(View.INVISIBLE);
          }
        });
    collapsingToolbarLayout.setTitle(userDetailModel.getFullname());
    editTextFullName.setText(userDetailModel.getFullname());
    editTextEmail.setText(userDetailModel.getEmail());
    editTextWebsite.setText(userDetailModel.getWebsite());
    editTextAddress.setText(userDetailModel.getAddress());
    editTextPhone.setText(userDetailModel.getPhone());
    editTextCompany.setText(userDetailModel.getCompany());
  }

  @Override public void getUserDetailsError(String errorMessage) {
    longToast(errorMessage);
  }

  @Override public void updateUserProfileSuccess() {
    longToast(String.format(getStringShort(R.string.successfully_updated_profile),
        editTextFullName.getText().toString()));
  }

  @Override public void updateUserProfileError(String errorMessage) {
    longToast(errorMessage);
  }

  @Override public void navigateBack() {
    super.onBackPressed();
  }

  @Override public void displayPleaseWait() {
    showPleaseWaitDialog(getStringShort(R.string.getting_profile_please_wait));
  }

  @Override public void displayPleaseWaitForUpdate() {
    showPleaseWaitDialog(String.format(getStringShort(R.string.updating_profile_please_wait),
        editTextFullName.getText().toString()));
  }

  @Override public void hidePleaseWait() {
    hidePleaseWaitDialog();
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        onBackPressed();
        return true;
    }

    return (super.onOptionsItemSelected(item));
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    userDetailPresenter.stop();
  }

  @Override public void onBackPressed() {
    userDetailPresenter.checkChanges(editTextWebsite.getText().toString(),
        editTextAddress.getText().toString(), editTextPhone.getText().toString(),
        editTextCompany.getText().toString());
  }
}
