package com.yasinkacmaz.foodusers.login;

import com.yasinkacmaz.foodusers.constants.NetworkConstants;
import com.yasinkacmaz.foodusers.service.FoodApi;
import javax.inject.Inject;
import io.reactivex.Observable;
import retrofit2.Response;

public class LoginModel implements LoginContract.Model {

  private FoodApi foodApi;

  @Inject public LoginModel(FoodApi foodApi) {
    this.foodApi = foodApi;
  }

  @Override public Observable<Response<LoginResponse>> loginUser(String username, String password) {
    return foodApi.loginUser(username + "@" + username, password, NetworkConstants.DELAY_SECONDS);
  }
}
