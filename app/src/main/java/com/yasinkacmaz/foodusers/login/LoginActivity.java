package com.yasinkacmaz.foodusers.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.widget.EditText;
import com.yasinkacmaz.foodusers.R;
import com.yasinkacmaz.foodusers.base.BaseActivity;
import com.yasinkacmaz.foodusers.users.UsersActivity;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity implements LoginContract.View {

  @BindView(R.id.toolbar) Toolbar toolbar;
  @BindView(R.id.login_username) EditText editTextUsername;
  @BindView(R.id.login_password) EditText editTextPassword;
  @BindView(R.id.login_activity_login_button) CardView cardViewLogin;

  @Inject LoginContract.Presenter loginPresenter;

  @OnClick(R.id.login_activity_login_button) public void loginClick() {
    loginPresenter.onLoginClicked(editTextUsername.getText().toString(),
        editTextPassword.getText().toString());
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);

    ButterKnife.bind(this);

    setSupportActionBar(toolbar);
    if (getSupportActionBar() != null) {
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    loginPresenter.start();

    initViews();
  }

  public void initViews() {
    TextWatcher textWatcher = new TextWatcher() {
      @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
      }

      @Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
      }

      @Override public void afterTextChanged(Editable editable) {
        loginPresenter.checkFields(editTextUsername.getText().toString(),
            editTextPassword.getText().toString());
      }
    };

    editTextUsername.addTextChangedListener(textWatcher);
    editTextPassword.addTextChangedListener(textWatcher);
  }

  @Override public void toggleLoginButton(boolean shouldEnable) {
    cardViewLogin.setClickable(shouldEnable);
    cardViewLogin.setEnabled(shouldEnable);
    cardViewLogin.setAlpha(shouldEnable?1f:0.5f);
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    loginPresenter.stop();
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        onBackPressed();
        return true;
    }

    return (super.onOptionsItemSelected(item));
  }

  @Override public void onBackPressed() {
    moveTaskToBack(true);
  }

  @Override public void handleLoginSuccess() {
    shortToast(R.string.login_successful);
    startActivity(new Intent(thisActivity, UsersActivity.class));
  }

  @Override public void handleLoginError(String errorMessage) {
    longToast(errorMessage);
  }

  @Override public void displayPleaseWait() {
    showPleaseWaitDialog(getStringShort(R.string.logging_in_please_wait));
  }

  @Override public void hidePleaseWait() {
    hidePleaseWaitDialog();
  }
}
