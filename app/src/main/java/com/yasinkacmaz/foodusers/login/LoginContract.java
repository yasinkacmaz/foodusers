package com.yasinkacmaz.foodusers.login;

import com.yasinkacmaz.foodusers.base.BasePresenter;
import com.yasinkacmaz.foodusers.base.BaseView;
import io.reactivex.Observable;
import retrofit2.Response;

public interface LoginContract {

  interface View extends BaseView {
    void toggleLoginButton(boolean shouldEnable);

    void handleLoginSuccess();

    void handleLoginError(String errorMessage);
  }

  interface Presenter extends BasePresenter {
    void checkFields(String username, String password);

    void onLoginClicked(String username, String password);
  }

  interface Model {
    Observable<Response<LoginResponse>> loginUser(String username, String password);
  }
}
