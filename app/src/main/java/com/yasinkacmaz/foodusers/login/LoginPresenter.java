package com.yasinkacmaz.foodusers.login;

import com.yasinkacmaz.foodusers.base.BasePresenterImpl;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import javax.inject.Inject;

public class LoginPresenter extends BasePresenterImpl implements LoginContract.Presenter {

  private LoginContract.View loginView;
  private LoginContract.Model loginModel;

  @Inject public LoginPresenter(LoginContract.View view, LoginContract.Model service) {
    this.loginView = view;
    this.loginModel = service;
  }

  @Override public void start() {
    super.start();
    loginView.toggleLoginButton(false);
  }

  @Override public void onLoginClicked(String username, String password) {
    loginModel.loginUser(username, password)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .doOnSubscribe(d -> {
          loginView.displayPleaseWait();
          addDisposable(d);
        })
        .subscribe(response -> {
          loginView.hidePleaseWait();
          if (response.isSuccessful()) {
            loginView.handleLoginSuccess();
          } else {
            loginView.handleLoginError(response.errorBody().string());
          }
        }, error -> {
          loginView.hidePleaseWait();
          loginView.handleLoginError(error.getMessage());
        }, () -> {
        });
  }

  @Override public void checkFields(String username, String password) {
    boolean result = !username.isEmpty() && !password.isEmpty();
    loginView.toggleLoginButton(result);
  }
}
