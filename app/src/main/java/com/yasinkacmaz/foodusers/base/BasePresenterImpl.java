package com.yasinkacmaz.foodusers.base;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public abstract class BasePresenterImpl implements BasePresenter{
  CompositeDisposable disposable;

  @Override public void start() {
    disposable = new CompositeDisposable();
  }

  @Override public void stop() {
    disposable.clear();
  }

  @Override public void addDisposable(Disposable d) {
    disposable.add(d);
  }
}
