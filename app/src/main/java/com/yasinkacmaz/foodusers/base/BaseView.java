package com.yasinkacmaz.foodusers.base;

public interface BaseView {
  void displayPleaseWait();

  void hidePleaseWait();
}
