package com.yasinkacmaz.foodusers.base;

import io.reactivex.disposables.Disposable;

public interface BasePresenter {
  void addDisposable(Disposable d);

  void start();

  void stop();
}
