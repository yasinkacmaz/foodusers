package com.yasinkacmaz.foodusers.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;
import com.afollestad.materialdialogs.MaterialDialog;
import com.yasinkacmaz.foodusers.R;
import dagger.android.AndroidInjection;

public class BaseActivity extends AppCompatActivity {

  public AppCompatActivity thisActivity = this;
  public MaterialDialog mPleaseWaitDialog;

  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    AndroidInjection.inject(this);
  }

  @Override protected void onPause() {
    hidePleaseWaitDialog();
    super.onPause();
  }

  public String getStringShort(int string) {
    return getApplicationContext().getString(string);
  }

  public int getColorShort(int color) {
    return ContextCompat.getColor(getApplicationContext(), color);
  }

  public void shortToast(int stringId) {
    Toast.makeText(getApplicationContext(), getStringShort(stringId), Toast.LENGTH_SHORT).show();
  }

  public void longToast(int stringId) {
    Toast.makeText(getApplicationContext(), getStringShort(stringId), Toast.LENGTH_LONG).show();
  }

  public void longToast(String message) {
    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
  }

  public void hidePleaseWaitDialog() {
    if (mPleaseWaitDialog != null && mPleaseWaitDialog.isShowing()) mPleaseWaitDialog.dismiss();
  }

  public void showPleaseWaitDialog(@Nullable String message) {
    mPleaseWaitDialog = new MaterialDialog.Builder(thisActivity).content(
        message == null ? getStringShort(R.string.please_wait) : message)
        .progress(true, 0)
        .itemsColor(getColorShort(R.color.colorAccent))
        .widgetColor(getColorShort(R.color.colorAccent))
        .canceledOnTouchOutside(false)
        .build();
    mPleaseWaitDialog.show();
  }
}
