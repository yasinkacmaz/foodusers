package com.yasinkacmaz.foodusers.users;

import com.yasinkacmaz.foodusers.service.FoodApi;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class) public class UsersServiceTest {
  private TestObserver<Response<UsersResponse>> testObserver;
  private Response<UsersResponse> responseSuccess;
  private Response<UsersResponse> responseError;

  @Mock private ResponseBody errorBody;
  @Mock private FoodApi foodApi;

  private UsersModel usersModel;

  @Before public void setUp() throws Exception {
    usersModel = Mockito.spy(new UsersModel(foodApi));
    responseSuccess = Response.success(new UsersResponse());
    responseError = Response.error(400, errorBody);
    testObserver = new TestObserver<>();
  }

  @After public void tearDown() throws Exception {
    testObserver.dispose();
  }

  @Test public void testGetUsersSuccess() throws Exception {
    when(foodApi.getUsers(anyInt(), anyInt())).thenReturn(Observable.just(responseSuccess));

    testObserver = usersModel.getUsers().test();

    testObserver.assertSubscribed();
    testObserver.assertValue(responseSuccess);
    testObserver.assertNoErrors();
    testObserver.assertComplete();
  }

  @Test public void testGetUsersError() throws Exception {
    when(foodApi.getUsers(anyInt(), anyInt())).thenReturn(Observable.just(responseError));

    testObserver = usersModel.getUsers().test();

    testObserver.assertSubscribed();
    testObserver.assertValue(responseError);
    testObserver.assertNoErrors();
    testObserver.assertComplete();
  }

  @Test public void testGetUsersFail() throws Exception {
    Throwable error = new Throwable();
    when(foodApi.getUsers(anyInt(), anyInt())).thenReturn(Observable.error(error));

    testObserver = usersModel.getUsers().test();

    testObserver.assertSubscribed();
    testObserver.assertNoValues();
    testObserver.assertError(error);
    testObserver.assertNotComplete();
  }
}