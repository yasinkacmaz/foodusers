package com.yasinkacmaz.foodusers.users;

import com.yasinkacmaz.foodusers.TestUtils;
import io.reactivex.Observable;
import java.util.ArrayList;
import java.util.List;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import retrofit2.Response;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class) public class UsersPresenterTest {
  @Mock private UsersContract.Model usersModel;
  @Mock private UsersContract.View usersView;
  private ResponseBody errorBody;
  private String errorString = "error";

  private UsersPresenter usersPresenter;

  @Before public void setUp() throws Exception {
    TestUtils.hookTestRxJavaSchedulers();
    errorBody = ResponseBody.create(MediaType.parse("text/plain"), errorString);
    usersPresenter = new UsersPresenter(usersView, usersModel);

    usersPresenter.start();
  }

  @After public void tearDown() throws Exception {
    usersPresenter.stop();
  }

  @Test public void testGetUsersSuccess() throws Exception {
    UsersResponse usersResponse = new UsersResponse();
    List<User> models = new ArrayList<>();
    usersResponse.setData(models);
    Response<UsersResponse> response = Response.success(usersResponse);
    when(usersModel.getUsers()).thenReturn(Observable.just(response));

    usersPresenter.getUsers();

    verify(usersView).getUsersSuccess(models);
  }

  @Test public void testGetUsersError() throws Exception {
    Response<UsersResponse> errorResponse = Response.error(400, errorBody);
    when(usersModel.getUsers()).thenReturn(Observable.just(errorResponse));

    usersPresenter.getUsers();

    verify(usersView).getUsersError(errorString);
  }

  @Test public void testGetUsersFail() throws Exception {
    String failString = "failed";
    when(usersModel.getUsers()).thenReturn(Observable.error(new Throwable(failString)));

    usersPresenter.getUsers();

    verify(usersView).getUsersError(failString);
  }
}