package com.yasinkacmaz.foodusers.userdetail;

import com.yasinkacmaz.foodusers.TestUtils;
import com.yasinkacmaz.foodusers.userdetail.response.UserDetailResponse;
import io.reactivex.Observable;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import retrofit2.Response;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class) public class UserDetailPresenterTest {

  @Mock private UserDetailContract.Model userDetailModel;
  @Mock private UserDetailContract.View userDetailView;
  private ResponseBody errorBody;
  private String errorString = "error";
  private UserDetailPresenter userDetailPresenter;

  @Before public void setUp() throws Exception {
    TestUtils.hookTestRxJavaSchedulers();
    errorBody = ResponseBody.create(MediaType.parse("text/plain"), errorString);
    userDetailPresenter = new UserDetailPresenter(userDetailView, userDetailModel);

    userDetailPresenter.start();
  }

  @After public void tearDown() throws Exception {
    userDetailPresenter.stop();
  }

  @Test public void testGetUserDetailsSuccess() throws Exception {
    UserDetail userDetailModel = new UserDetail();
    UserDetailResponse userDetailResponse = new UserDetailResponse();
    Response<UserDetailResponse> response = Response.success(userDetailResponse);
    when(this.userDetailModel.getUserDetail(anyInt())).thenReturn(Observable.just(response));
    when(this.userDetailModel.convertUserDetailToModel(any(UserDetailResponse.class))).thenReturn(
        Observable.just(userDetailModel));

    userDetailPresenter.getUserDetails();

    verify(this.userDetailModel).getUserDetail(anyInt());
    verify(this.userDetailModel).convertUserDetailToModel(userDetailResponse);
    verify(userDetailView).displayPleaseWait();
    verify(userDetailView).hidePleaseWait();
    verify(userDetailView).getUserDetailsSuccess(userDetailModel);
  }

  @Test public void testGetUserDetailsError() throws Exception {
    Response<UserDetailResponse> response = Response.error(400, errorBody);
    when(userDetailModel.getUserDetail(anyInt())).thenReturn(Observable.just(response));

    userDetailPresenter.getUserDetails();

    verify(userDetailModel).getUserDetail(anyInt());
    verify(userDetailModel, never()).convertUserDetailToModel(any(UserDetailResponse.class));
    verify(userDetailView).displayPleaseWait();
    verify(userDetailView).hidePleaseWait();
    verify(userDetailView).getUserDetailsError(errorString);
  }

  @Test public void testGetUserDetailsFail() throws Exception {
    Throwable throwable = new Throwable(errorString);
    when(userDetailModel.getUserDetail(anyInt())).thenReturn(Observable.error(throwable));

    userDetailPresenter.getUserDetails();

    verify(userDetailModel).getUserDetail(anyInt());
    verify(userDetailModel, never()).convertUserDetailToModel(any(UserDetailResponse.class));
    verify(userDetailView).displayPleaseWait();
    verify(userDetailView).hidePleaseWait();
    verify(userDetailView).getUserDetailsError(errorString);
  }

  @Test public void testCheckChanges_NoChanged() throws Exception {
    when(userDetailModel.isChanged(anyString(), anyString(), anyString(), anyString())).thenReturn(
        false);

    userDetailPresenter.checkChanges("", "", "", "");

    verify(userDetailView).navigateBack();
    verify(userDetailView, never()).displayPleaseWaitForUpdate();
    verify(userDetailModel, never()).updateUserProfile(anyString(), anyString(), anyString(),
        anyString());
  }

  @Test public void testCheckChanges_UpdateSuccess() throws Exception {
    when(userDetailModel.isChanged(anyString(), anyString(), anyString(), anyString())).thenReturn(
        true);
    String updateResult = "success";
    when(userDetailModel.updateUserProfile(anyString(), anyString(), anyString(),
        anyString())).thenReturn(Observable.just(updateResult));

    userDetailPresenter.checkChanges("", "", "", "");

    verify(userDetailView).displayPleaseWaitForUpdate();
    verify(userDetailModel).updateUserProfile(anyString(), anyString(), anyString(), anyString());
    verify(userDetailView).updateUserProfileSuccess();
    verify(userDetailView).hidePleaseWait();
    verify(userDetailView).navigateBack();
  }

  @Test public void testCheckChanges_UpdateError() throws Exception {
    Throwable throwable = new Throwable(errorString);
    when(userDetailModel.isChanged(anyString(), anyString(), anyString(), anyString())).thenReturn(
        true);
    when(userDetailModel.updateUserProfile(anyString(), anyString(), anyString(),
        anyString())).thenReturn(Observable.error(throwable));

    userDetailPresenter.checkChanges("", "", "", "");

    verify(userDetailView).displayPleaseWaitForUpdate();
    verify(userDetailModel).updateUserProfile(anyString(), anyString(), anyString(), anyString());
    verify(userDetailView).updateUserProfileError(errorString);
    verify(userDetailView).hidePleaseWait();
    verify(userDetailView).navigateBack();
  }
}