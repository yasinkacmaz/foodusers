package com.yasinkacmaz.foodusers.userdetail;

import com.yasinkacmaz.foodusers.service.UserApi;
import com.yasinkacmaz.foodusers.userdetail.response.UserDetailResponse;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class) public class UserDetailServiceTest {
  private TestObserver<Response<UserDetailResponse>> testObserverProfile;
  private TestObserver<UserDetail> testObserverProfileModel;
  private TestObserver<String> testObserverUpdate;

  @Mock private ResponseBody errorBody;
  @Mock private UserApi userApi;

  private UserDetailModel userDetailModel;

  @Before public void setUp() throws Exception {
    userDetailModel = Mockito.spy(new UserDetailModel(userApi));
    testObserverProfile = new TestObserver<>();
    testObserverProfileModel = new TestObserver<>();
    testObserverUpdate = new TestObserver<>();
  }

  @After public void tearDown() throws Exception {
    testObserverProfile.dispose();
    testObserverUpdate.dispose();
    testObserverProfileModel.dispose();
  }

  @Test public void testGetUserDetailSuccess() throws Exception {
    Response<UserDetailResponse> response = Response.success(new UserDetailResponse());
    when(userApi.getUserDetails(anyInt())).thenReturn(Observable.just(response));

    testObserverProfile = userDetailModel.getUserDetail(1).test();

    testObserverProfile.await();
    testObserverProfile.assertSubscribed();
    testObserverProfile.assertValue(response);
    testObserverProfile.assertNoErrors();
    testObserverProfile.assertComplete();
  }

  @Test public void testGetUserDetailError() throws Exception {
    Response<UserDetailResponse> response = Response.error(400, errorBody);
    when(userApi.getUserDetails(anyInt())).thenReturn(Observable.just(response));

    testObserverProfile = userDetailModel.getUserDetail(1).test();

    testObserverProfile.await();
    testObserverProfile.assertSubscribed();
    testObserverProfile.assertValue(response);
    testObserverProfile.assertNoErrors();
    testObserverProfile.assertComplete();
  }

  @Test public void testGetUserDetailFail() throws Exception {
    Throwable throwable = new Throwable();
    when(userApi.getUserDetails(anyInt())).thenReturn(Observable.error(throwable));

    testObserverProfile = userDetailModel.getUserDetail(1).test();

    testObserverProfile.assertSubscribed();
    testObserverProfile.assertNoValues();
    testObserverProfile.assertError(throwable);
    testObserverProfile.assertNotComplete();
  }

  @Test public void testConvertUserDetailToModel() throws Exception {

    testObserverProfileModel =
        userDetailModel.convertUserDetailToModel(new UserDetailResponse()).test();

    testObserverProfileModel.assertSubscribed();
    testObserverProfileModel.assertValue(userDetailModel.userDetail);
    testObserverProfileModel.assertNoErrors();
    testObserverProfileModel.assertComplete();
    assertNotNull(userDetailModel.userDetail);
  }

  @Test public void testIsChanged_DoNotHaveUserDetails() throws Exception {
    userDetailModel.userDetail = null;

    assertFalse(userDetailModel.isChanged("", "", "", ""));
  }

  @Test public void testIsChanged_NoneOfFieldsChanged() throws Exception {
    UserDetail userDetailModel = new UserDetail();
    userDetailModel.setCompany("");
    userDetailModel.setAddress("");
    userDetailModel.setPhone("");
    userDetailModel.setWebsite("");
    userDetailModel.setEmail("");
    userDetailModel.setFullname("");
    this.userDetailModel.userDetail = userDetailModel;

    assertFalse(this.userDetailModel.isChanged("", "", "", ""));
  }

  @Test public void testIsChanged_AnyOneOfFieldsChanges() throws Exception {
    UserDetail userDetailModel = new UserDetail();
    userDetailModel.setCompany("");
    userDetailModel.setAddress("");
    userDetailModel.setPhone("");
    userDetailModel.setWebsite("");
    userDetailModel.setEmail("");
    userDetailModel.setFullname("");
    this.userDetailModel.userDetail = userDetailModel;

    assertTrue(this.userDetailModel.isChanged("a", "", "", ""));
  }

  @Test public void testUpdateUserProfileSuccess() throws Exception {
    testObserverUpdate = userDetailModel.updateUserProfile("", "", "", "").test();

    // await unit test threadini blocklar
    // gereğinden fazla kullanırsan ci için fazladan süre harcanır
    // todo burası önemli daha sonra bak
    testObserverUpdate.await();
    testObserverUpdate.assertSubscribed();
    testObserverUpdate.assertValue("");
    testObserverUpdate.assertNoErrors();
    testObserverUpdate.assertComplete();
  }
}