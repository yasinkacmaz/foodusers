package com.yasinkacmaz.foodusers.base;

import io.reactivex.disposables.Disposable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class) public class BasePresenterImplTest {
  private BasePresenterImpl basePresenterImpl;
  @Mock private Disposable disposable;

  @Before public void setUp() throws Exception {
    basePresenterImpl = Mockito.spy(BasePresenterImpl.class);
  }

  @Test public void start() throws Exception {
    basePresenterImpl.start();
    assertNotNull(basePresenterImpl.disposable);
  }

  @Test public void stop() throws Exception {
    basePresenterImpl.start();
    basePresenterImpl.addDisposable(disposable);

    assertEquals(basePresenterImpl.disposable.size(), 1);
    basePresenterImpl.stop();
    assertEquals(basePresenterImpl.disposable.size(), 0);
  }

  @Test public void addDisposable() throws Exception {
    basePresenterImpl.start();
    basePresenterImpl.addDisposable(disposable);

    assertEquals(basePresenterImpl.disposable.size(), 1);
  }
}