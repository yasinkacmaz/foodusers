package com.yasinkacmaz.foodusers;

import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;

public class TestUtils {
  public static void hookTestRxJavaSchedulers() {
    RxAndroidPlugins.setMainThreadSchedulerHandler(__ -> Schedulers.trampoline());
    RxAndroidPlugins.setInitMainThreadSchedulerHandler(__ -> Schedulers.trampoline());
    RxJavaPlugins.setIoSchedulerHandler(__ -> Schedulers.trampoline());
    RxJavaPlugins.setNewThreadSchedulerHandler(__ -> Schedulers.trampoline());
    RxJavaPlugins.setComputationSchedulerHandler(__ -> Schedulers.trampoline());
    RxAndroidPlugins.setInitMainThreadSchedulerHandler(__ -> Schedulers.trampoline());
  }
}
