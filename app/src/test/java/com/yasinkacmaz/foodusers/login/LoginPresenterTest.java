package com.yasinkacmaz.foodusers.login;

import com.yasinkacmaz.foodusers.TestUtils;
import io.reactivex.Observable;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import retrofit2.Response;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class) public class LoginPresenterTest {

  @Mock private LoginContract.Model loginModel;
  @Mock private LoginContract.View loginView;
  private ResponseBody errorBody;
  private String errorString = "error";

  private LoginPresenter loginPresenter;

  @Before public void setUp() throws Exception {
    TestUtils.hookTestRxJavaSchedulers();
    errorBody = ResponseBody.create(MediaType.parse("text/plain"), errorString);
    loginPresenter = new LoginPresenter(loginView, loginModel);

    loginPresenter.start();
  }

  @After public void tearDown() throws Exception {
    loginPresenter.stop();
  }

  @Test public void testOnLoginClicked_ResponseSuccess() throws Exception {

    LoginResponse loginResponse = new LoginResponse();

    when(loginModel.loginUser(anyString(), anyString())).thenReturn(
        Observable.just(Response.success(loginResponse)));

    loginPresenter.onLoginClicked("", "");

    verify(loginView).displayPleaseWait();
    verify(loginView).hidePleaseWait();
    verify(loginView).handleLoginSuccess();
  }

  @Test public void testOnLoginClicked_ResponseError() throws Exception {

    when(loginModel.loginUser(anyString(), anyString())).thenReturn(
        Observable.just(Response.error(400, errorBody)));

    loginPresenter.onLoginClicked("", "");

    verify(loginView).displayPleaseWait();
    verify(loginView).hidePleaseWait();
    verify(loginView).handleLoginError(errorString);
  }

  @Test public void testOnLoginClicked_Failed() throws Exception {
    String failMessage = "message";

    when(loginModel.loginUser(anyString(), anyString())).thenReturn(
        Observable.error(new Throwable(failMessage)));

    loginPresenter.onLoginClicked("", "");

    verify(loginView).displayPleaseWait();
    verify(loginView).hidePleaseWait();
    verify(loginView).handleLoginError(failMessage);
  }

  /**
   * two time because this also called by { @link LoginPresenter#start()} ]
   */
  @Test public void testCheckFields_UsernameIsEmpty() throws Exception {
    loginPresenter.checkFields("", "password");

    verify(loginView, times(2)).toggleLoginButton(false);
  }

  @Test public void testCheckFields_PasswordIsEmpty() throws Exception {
    loginPresenter.checkFields("username", "");

    verify(loginView, times(2)).toggleLoginButton(false);
  }

  @Test public void testCheckFields_BothEmpty() throws Exception {
    loginPresenter.checkFields("", "");

    verify(loginView, times(2)).toggleLoginButton(false);
  }

  @Test public void testCheckFields_BothIsNotEmpty() throws Exception {
    loginPresenter.checkFields("username", "password");

    verify(loginView).toggleLoginButton(true);
  }
}