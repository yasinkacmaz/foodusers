package com.yasinkacmaz.foodusers.login;

import com.yasinkacmaz.foodusers.service.FoodApi;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class) public class LoginServiceTest {
  private TestObserver<Response<LoginResponse>> testObserver;
  private Response<LoginResponse> responseSuccess;
  private Response<LoginResponse> responseError;

  @Mock private ResponseBody errorBody;
  @Mock private FoodApi foodApi;

  private LoginModel loginModel;

  @Before public void setUp() throws Exception {
    loginModel = Mockito.spy(new LoginModel(foodApi));
    responseSuccess = Response.success(new LoginResponse());
    responseError = Response.error(400, errorBody);
    testObserver = new TestObserver<>();
  }

  @After public void tearDown() throws Exception {
    testObserver.dispose();
  }

  @Test public void testLoginUser_Success() throws Exception {
    when(foodApi.loginUser(anyString(), anyString(), anyInt())).thenReturn(
        Observable.just(responseSuccess));

    testObserver = loginModel.loginUser("", "").test();

    testObserver.assertSubscribed();
    testObserver.assertValue(responseSuccess);
    testObserver.assertNoErrors();
    testObserver.assertComplete();
  }

  @Test public void testLoginUser_Error() throws Exception {
    when(foodApi.loginUser(anyString(), anyString(), anyInt())).thenReturn(
        Observable.just(responseError));

    testObserver = loginModel.loginUser("", "").test();

    testObserver.assertSubscribed();
    testObserver.assertValue(responseError);
    testObserver.assertNoErrors();
    testObserver.assertComplete();
  }

  @Test public void testLoginUser_Fail() throws Exception {
    Throwable error = new Throwable();
    when(foodApi.loginUser(anyString(), anyString(), anyInt())).thenReturn(Observable.error(error));

    testObserver = loginModel.loginUser("", "").test();

    testObserver.assertSubscribed();
    testObserver.assertNoValues();
    testObserver.assertError(error);
    testObserver.assertNotComplete();
  }
}